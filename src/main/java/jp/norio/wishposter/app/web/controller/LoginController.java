package jp.norio.wishposter.app.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * The controller for login pages
 */
@Controller
public class LoginController extends AbstractController{
	
	/**
	 * The logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);
	
	/**
	 * The path of login page
	 */
	public static final String PATH_LOGIN  = "/login";

	/**
	 * The view of login page
	 */
	public static final String VIEW_LOGIN  = "login";

	/**
	 * The method for login page
	 * 
	 * @return The view of this method
	 */
	@RequestMapping(path=PATH_LOGIN, method=RequestMethod.GET)
	String loginForm(){
		return VIEW_LOGIN;
	}
}
