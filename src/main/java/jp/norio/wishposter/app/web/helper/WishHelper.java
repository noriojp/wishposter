package jp.norio.wishposter.app.web.helper;

import org.springframework.stereotype.Component;

import jp.norio.wishposter.app.web.form.WishAdditionForm;
import jp.norio.wishposter.domain.entity.Wish;
import jp.norio.wishposter.domain.entity.Wisher;

/**
 * The helper for wish
 */
@Component
public class WishHelper {

	/**
	 * Return new {@link Wish} instance created with {@link WishAdditionForm} instance
	 * 
	 * @param wishForm The {@link WishAdditionForm} instance
	 * @param wisher The {@link Wisher} who post the wish
	 * @return New {@link Wish} instance
	 */
	public Wish newEntity(WishAdditionForm wishForm, Wisher wisher){
		return new Wish()
				.name(wishForm.getName())
				.quantity(wishForm.getQuantity().intValue())
				.priority(wishForm.getPriority())
				.comment(wishForm.getComment())
				.wisher(wisher)
				.version(1);
	}
}
