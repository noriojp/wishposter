package jp.norio.wishposter.app.web.helper;

import java.sql.Timestamp;

import org.springframework.stereotype.Component;

import jp.norio.wishposter.app.web.form.TaskAdditionForm;
import jp.norio.wishposter.domain.entity.Task;
import jp.norio.wishposter.domain.entity.Wisher;

/**
 * The helper for task
 */
@Component
public class TaskHelper {

	/**
	 * Return new {@link Task} instance created with {@link TaskAdditionForm} instance
	 * 
	 * @param taskForm The {@link TaskAdditionForm} instance
	 * @param wisher The {@link Wisher} who post the task
	 * @return New {@link Task} instance
	 */
	public Task newEntity(TaskAdditionForm taskForm, Wisher wisher){
		return new Task()
				.name(taskForm.getName())
				.priority(taskForm.getPriority())
				.deadline(new Timestamp(taskForm.getDeadline().getTime()))
				.comment(taskForm.getComment())
				.wisher(wisher)
				.version(1);
	}
}
