package jp.norio.wishposter.app.web.viewhelper.bootstrap;

/**
 * Constants of The Bootstrap
 */
public class BootstrapConstants {

	/**
	 * The concatenator
	 */
	public static final String CONCATENATOR = "-";
	
	/**
	 * The panel component
	 */
	public static final String COMPONENT_PANEL = "panel";
	
	/**
	 * The label component
	 */
	public static final String COMPONENT_LABEL = "label";
	
	/**
	 * The button component
	 */
	public static final String COMPONENT_BUTTON = "btn";

	/**
	 * The color for danger
	 */
	public static final String COLOR_DANGER = "danger";

	/**
	 * The color for warning
	 */
	public static final String COLOR_WARNING = "warning";

	/**
	 * The color for information
	 */
	public static final String COLOR_INFO = "info";

	/**
	 * The color for success
	 */
	public static final String COLOR_SUCCESS = "success";

	/**
	 * The color for primary
	 */
	public static final String COLOR_PRIMARY = "primary";

	/**
	 * The color for default
	 */
	public static final String COLOR_DEFAULT = "default";

	/**
	 * No instance
	 */
	private BootstrapConstants() {
	}
	
}
