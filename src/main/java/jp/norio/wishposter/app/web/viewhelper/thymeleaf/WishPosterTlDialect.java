package jp.norio.wishposter.app.web.viewhelper.thymeleaf;

import org.springframework.stereotype.Component;
import org.thymeleaf.dialect.AbstractDialect;
import org.thymeleaf.dialect.IExpressionObjectDialect;
import org.thymeleaf.expression.IExpressionObjectFactory;

/**
 * The Thymeleaf dialect for Wish Poster application
 */
@Component
public class WishPosterTlDialect extends AbstractDialect implements IExpressionObjectDialect {

	/** This dialect's name */
    public static final String NAME = "WishPoster";
    /** The factory instance */
    private IExpressionObjectFactory expressionObjectFactory = null;
    
    /**
     * Default constructor
     */
    public WishPosterTlDialect() {
    	this(NAME);
	}
    
    /**
     * Constructor
     * 
     * @param name dialect name
     */
	protected WishPosterTlDialect(String name) {
		super(name);
	}


	@Override
	public IExpressionObjectFactory getExpressionObjectFactory() {
		if (expressionObjectFactory == null) {
			expressionObjectFactory = new WishPosterTlExpressionObjectFactory();
		}
		return expressionObjectFactory;
	}

}
