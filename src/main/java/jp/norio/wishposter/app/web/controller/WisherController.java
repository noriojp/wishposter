package jp.norio.wishposter.app.web.controller;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import jp.norio.wishposter.app.web.form.WisherAdditionForm;
import jp.norio.wishposter.app.web.helper.WisherHelper;
import jp.norio.wishposter.domain.Pagination;
import jp.norio.wishposter.domain.Role;
import jp.norio.wishposter.domain.annotation.LoginUser;
import jp.norio.wishposter.domain.entity.LoginUserDetails;
import jp.norio.wishposter.domain.entity.Wisher;
import jp.norio.wishposter.domain.service.WisherService;

/**
 * The controller for wisher pages
 */
@Controller
public class WisherController extends AbstractController{

	/**
	 * The logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(WisherController.class);

	/**
	 * The path of index page
	 */
	public static final String PATH_INDEX = "/wisher/";

	/**
	 * The path of addition page
	 */
	public static final String PATH_ADD = "/wisher/add";

	/**
	 * The view of index page
	 */
	public static final String VIEW_INDEX = "wisher/index";

	/**
	 * The view of addition page
	 */
	public static final String VIEW_ADD = "wisher/add";

	/**
	 * The {@link WisherService}
	 */
	@Autowired
	WisherService wisherService;
	
	/**
	 * The {@link WisherHelper}
	 */
	@Autowired
	WisherHelper wisherHelper;

	/**
	 * The method for index page
	 * 
	 * @param pageable The {@link Pageable} instance for this index
	 * @param mav The {@link ModelAndView} instance for this method
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_INDEX)
	ModelAndView index(@PageableDefault(page=0, size=10) Pageable pageable, ModelAndView mav){
		Pagination<Wisher> wishers = new Pagination<>(wisherService.getWishersInPage(pageable.previousOrFirst()), PATH_INDEX);
		
		mav.setViewName(VIEW_INDEX);
		mav.addObject("wishers", wishers);
		
		return mav;
	}

	/**
	 * The method for addition form page
	 * 
	 * @param wisherAdditionForm The {@link WisherAdditionForm} instance
	 * @param mav The {@link ModelAndView} instance for this method
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_ADD, method=RequestMethod.GET)
	ModelAndView addForm(@ModelAttribute("wisherAdditionForm") WisherAdditionForm wisherAdditionForm, ModelAndView mav){
		
		mav.setViewName(VIEW_ADD);
		
		// Build Role list
		List<Role> roles = Arrays.asList(Role.values());
		mav.addObject("roles", roles);
		
		// Set default Role
		if (wisherAdditionForm.getRole() == null) {
			wisherAdditionForm.setRole(Role.USER);
		}
		
		return mav;
	}

	/**
	 * The method for addition form action
	 * 
	 * @param wisherAdditionForm The {@link WisherAdditionForm} instance
	 * @param br The {@link BindingResult} instance
	 * @param mav The {@link ModelAndView} instance for this method
	 * @param loginUserDetails The {@link LoginUserDetails} of current login user
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_ADD, method=RequestMethod.POST)
	@Transactional(readOnly=false)
	ModelAndView add(@ModelAttribute("wisherAdditionForm") @Validated WisherAdditionForm wisherAdditionForm, BindingResult br, ModelAndView mav, @LoginUser LoginUserDetails loginUserDetails){

		if (br.hasErrors()) {
			// Back to the form page
			return addForm(wisherAdditionForm, mav);
		}else {
			// Save the new wisher
			wisherService.saveAndFlush(wisherHelper.newEntity(wisherAdditionForm));
			
			// Back to index page
			return new ModelAndView(redirect(PATH_INDEX));
		}
	}

}
