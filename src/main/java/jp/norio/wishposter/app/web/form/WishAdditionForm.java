package jp.norio.wishposter.app.web.form;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


import jp.norio.wishposter.domain.Priority;
import jp.norio.wishposter.validator.constraints.PriorityEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * The form for wish addition
 */
@Data
@Accessors(fluent=false)
public class WishAdditionForm {

	/**
	 * The wish name
	 */
	@NotEmpty
	@Size(max=255)
	private String name;

	/**
	 * Quantity of the wish
	 */
	@NotNull
	@Min(1)
	private Integer quantity;

	/**
	 * Priority of the wish
	 */
	@NotNull
	@PriorityEnum
	@Enumerated(EnumType.STRING)
	private Priority priority;

	/**
	 * Comment for wish
	 */
	@NotNull
	@Size(max=255)
	private String comment;
}
