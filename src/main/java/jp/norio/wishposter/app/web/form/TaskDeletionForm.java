package jp.norio.wishposter.app.web.form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * The form for task deletion
 */
@Data
@Accessors(fluent=false)
public class TaskDeletionForm {

	/**
	 * The task ID
	 */
	@NotNull
	@Min(1)
	private Long id;

}
