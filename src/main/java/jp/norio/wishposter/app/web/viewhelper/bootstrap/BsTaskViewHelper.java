package jp.norio.wishposter.app.web.viewhelper.bootstrap;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import jp.norio.wishposter.domain.Priority;
import lombok.NonNull;

/**
 * The view helper for task with The Bootstrap
 */
public class BsTaskViewHelper {

	/**
	 * The map for priorities and colors
	 */
	@SuppressWarnings("serial")
	private static final Map<Priority, String> PRIORITY_COLOR_MAP = 
		Collections.unmodifiableMap(new HashMap<Priority, String>(){
			{
				put(Priority.CRITICAL, BootstrapConstants.COLOR_DANGER);
				put(Priority.HIGH, BootstrapConstants.COLOR_WARNING);
				put(Priority.MEDIUM, BootstrapConstants.COLOR_INFO);
				put(Priority.LOW, BootstrapConstants.COLOR_SUCCESS);
			}
	});

	/**
	 * Return the colored panel class with priority argument
	 * 
	 * @param priority The {@link Priority} for panel color
	 * @return The colored panel class
	 */
	public String panelClass(@NonNull Priority priority) {
		return BootstrapConstants.COMPONENT_PANEL + 
				BootstrapViewHelper.suffixOf(PRIORITY_COLOR_MAP.get(priority));
	}
	
	/**
	 * Return the colored button class with priority argument
	 * 
	 * @param priority The {@link Priority} for button color
	 * @return The colored button class
	 */
	public String buttonClass(@NonNull Priority priority) {
		return BootstrapConstants.COMPONENT_BUTTON + 
				BootstrapViewHelper.suffixOf(PRIORITY_COLOR_MAP.get(priority));
	}

}
