package jp.norio.wishposter.app.web.viewhelper.bootstrap;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import jp.norio.wishposter.domain.Role;
import lombok.NonNull;

/**
 * The view helper for wisher with The Bootstrap
 */
public class BsWisherViewHelper {

	/**
	 * The map for roles and colors
	 */
	@SuppressWarnings("serial")
	private static final Map<Role, String> ROLE_COLOR_MAP = 
		Collections.unmodifiableMap(new HashMap<Role, String>(){
			{
				put(Role.ADMIN, BootstrapConstants.COLOR_DANGER);
				put(Role.USER, BootstrapConstants.COLOR_INFO);
			}
	});
	
	/**
	 * Return the colored panel class with role argument
	 * 
	 * @param role The {@link Role} for panel class
	 * @return The colored panel class
	 */
	public String panelClass(@NonNull Role role) {
		return BootstrapConstants.COMPONENT_PANEL + 
				BootstrapViewHelper.suffixOf(ROLE_COLOR_MAP.get(role));
	}
	
	/**
	 * Return the colored label class with role argument
	 * 
	 * @param role The {@link Role} for label class
	 * @return The colored label class
	 */
	public String labelClass(@NonNull Role role) {
		return BootstrapConstants.COMPONENT_LABEL + 
				BootstrapViewHelper.suffixOf(ROLE_COLOR_MAP.get(role));
	}
	
	/**
	 * Return the colored button class with role argument
	 * 
	 * @param role The {@link Role} for button class
	 * @return The colored button class
	 */
	public String buttonClass(@NonNull Role role) {
		return BootstrapConstants.COMPONENT_BUTTON + 
				BootstrapViewHelper.suffixOf(ROLE_COLOR_MAP.get(role));
	}

}
