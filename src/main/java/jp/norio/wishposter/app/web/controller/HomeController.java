package jp.norio.wishposter.app.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * The controller for home page
 */
@Controller
public class HomeController extends AbstractController{
	
	/**
	 * The logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

	/**
	 * The path of home page
	 */
	public static final String PATH_HOME = "/";

	/**
	 * The view of home page
	 */
	public static final String VIEW_HOME = "index";

	/**
	 * The method for home page
	 * 
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_HOME)
	ModelAndView home(){
		// Forward to the wish index page
		return new ModelAndView(forward(WishController.PATH_INDEX));
	}
}
