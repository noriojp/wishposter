package jp.norio.wishposter.app.web.controller;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import jp.norio.wishposter.app.web.form.TaskAdditionForm;
import jp.norio.wishposter.app.web.form.TaskDeletionForm;
import jp.norio.wishposter.app.web.helper.TaskHelper;
import jp.norio.wishposter.domain.Pagination;
import jp.norio.wishposter.domain.Priority;
import jp.norio.wishposter.domain.annotation.LoginUser;
import jp.norio.wishposter.domain.entity.LoginUserDetails;
import jp.norio.wishposter.domain.entity.Task;
import jp.norio.wishposter.domain.service.TaskService;

/**
 * The controller for task pages
 */
@Controller
public class TaskController extends AbstractController {
	
	/**
	 * The logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(TaskController.class);

	/**
	 * The path of index page
	 */
	public static final String PATH_INDEX = "/task/";

	/**
	 * The path of addition page
	 */
	public static final String PATH_ADD = "/task/add";

	/**
	 * The path of deletion page
	 */
	public static final String PATH_DELETE = "/task/delete";

	/**
	 * The view of index page
	 */
	public static final String VIEW_INDEX = "task/index";

	/**
	 * The view of addition page
	 */
	public static final String VIEW_ADD = "task/add";

	/**
	 * The {@link TaskService}
	 */
	@Autowired
	TaskService taskService;
	
	/**
	 * The {@link TaskHelper}
	 */
	@Autowired
	TaskHelper taskHelper;
	
	/**
	 * The method for index page
	 * 
	 * @param pageable The {@link Pageable} instance for this index
	 * @param mav The {@link ModelAndView} instance for this method
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_INDEX)
	ModelAndView index(@PageableDefault(page=0, size=10) Pageable pageable, ModelAndView mav){
		Pagination<Task> tasks = new Pagination<>(taskService.getTasksInPage(pageable.previousOrFirst()), PATH_INDEX);
		
		mav.setViewName(VIEW_INDEX);
		mav.addObject("tasks", tasks);
		
		return mav;
	}

	/**
	 * The method for addition form page
	 * 
	 * @param taskAdditionForm The {@link TaskAdditionForm} instance
	 * @param mav The {@link ModelAndView} instance for this method
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_ADD, method=RequestMethod.GET)
	ModelAndView addForm(@ModelAttribute("taskAdditionForm") TaskAdditionForm taskAdditionForm, ModelAndView mav){
		
		mav.setViewName(VIEW_ADD);
		
		// Build Priority list
		List<Priority> priorities = Arrays.asList(Priority.values());
		Collections.reverse(priorities);
		mav.addObject("priorities", priorities);
		
		// Set default Priority
		if (taskAdditionForm.getPriority() == null) {
			taskAdditionForm.setPriority(Priority.MEDIUM);
		}
		
		return mav;
	}
	
	/**
	 * The method for addition form action
	 * 
	 * @param taskAdditionForm The {@link TaskAdditionForm} instance
	 * @param br The {@link BindingResult} instance
	 * @param mav The {@link ModelAndView} instance for this method
	 * @param loginUserDetails The {@link LoginUserDetails} of current login user
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_ADD, method=RequestMethod.POST)
	@Transactional(readOnly=false)
	ModelAndView add(@ModelAttribute("taskAdditionForm") @Validated TaskAdditionForm taskAdditionForm, BindingResult br, ModelAndView mav, @LoginUser LoginUserDetails loginUserDetails){

		if (br.hasErrors()) {
			// Back to the form page
			return addForm(taskAdditionForm, mav);
		}else {
			// Save the new task
			taskService.saveAndFlush(taskHelper.newEntity(taskAdditionForm, loginUserDetails.getWisher()));
			
			// Back to index page
			return new ModelAndView(redirect(PATH_INDEX));
		}
	}

	/**
	 * The method for deletion form action
	 * 
	 * @param taskDeletionForm The {@link TaskDeletionForm} instance
	 * @param br The {@link BindingResult} instance
	 * @param mav The {@link ModelAndView} instance for this method
	 * @param loginUserDetails The {@link LoginUserDetails} of current login user
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_DELETE, method=RequestMethod.POST)
	@Transactional(readOnly=false)
	ModelAndView delete(@ModelAttribute("taskDeletionForm") @Validated TaskDeletionForm taskDeletionForm, BindingResult br, ModelAndView mav, @LoginUser LoginUserDetails loginUserDetails){
		
		if (br.hasErrors()) {
			// No validation error occurred in normal operations
			return new ModelAndView(redirectToError());
		}
		
		// Delete the task
		taskService.delete(taskDeletionForm.getId());
		
		// Back to index page
		return new ModelAndView(redirect(PATH_INDEX));
	}
	
}
