package jp.norio.wishposter.app.web.helper;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import jp.norio.wishposter.app.web.form.WisherAdditionForm;
import jp.norio.wishposter.domain.entity.Wisher;

/**
 * The helper for wisher
 */
@Component
public class WisherHelper {

	/**
	 * Return new {@link Wisher} instance created with {@link WisherAdditionForm} instance
	 * 
	 * @param wisherForm The {@link Wisher} instance
	 * @return New {@link Wisher} instance
	 */
	public Wisher newEntity(WisherAdditionForm wisherForm){
		return new Wisher()
				.loginId(wisherForm.getLoginId())
				.encodedPassword(encodePassword(wisherForm.getPassword()))
				.name(wisherForm.getName())
				.role(wisherForm.getRole())
				.version(1);
	}

	/**
	 * Return the encoded password
	 * 
	 * @param password The password which you want to encode
	 * @return The encoded password
	 */
	public String encodePassword(String password) {
		return new BCryptPasswordEncoder().encode(password);
	}
}
