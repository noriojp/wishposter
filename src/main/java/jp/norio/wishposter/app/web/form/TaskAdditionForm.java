package jp.norio.wishposter.app.web.form;

import java.util.Date;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import jp.norio.wishposter.domain.Priority;
import jp.norio.wishposter.validator.constraints.PriorityEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * The form for task addition
 */
@Data
@Accessors(fluent=false)
public class TaskAdditionForm {

	/**
	 * The task name
	 */
	@NotEmpty
	@Size(max=255)
	private String name;

	/**
	 * Priority of the task
	 */
	@NotNull
	@PriorityEnum
	@Enumerated(EnumType.STRING)
	private Priority priority;

	/**
	 * Deadline of the task
	 */
	@NotNull
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Future
	private Date deadline;

	/**
	 * Comment for the task
	 */
	@NotNull
	@Size(max=255)
	private String comment;
}
