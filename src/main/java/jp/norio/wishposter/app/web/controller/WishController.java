package jp.norio.wishposter.app.web.controller;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import jp.norio.wishposter.app.web.form.WishAdditionForm;
import jp.norio.wishposter.app.web.form.WishDeletionForm;
import jp.norio.wishposter.app.web.helper.WishHelper;
import jp.norio.wishposter.domain.Pagination;
import jp.norio.wishposter.domain.Priority;
import jp.norio.wishposter.domain.annotation.LoginUser;
import jp.norio.wishposter.domain.entity.LoginUserDetails;
import jp.norio.wishposter.domain.entity.Wish;
import jp.norio.wishposter.domain.service.WishService;

/**
 * The controller for wish pages
 */
@Controller
public class WishController extends AbstractController{
	
	/**
	 * The logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(WishController.class);

	/**
	 * The path of index page
	 */
	public static final String PATH_INDEX = "/wish/";

	/**
	 * The path of addition page
	 */
	public static final String PATH_ADD = "/wish/add";
	
	/**
	 * The path of deletion page
	 */
	public static final String PATH_DELETE = "/wish/delete";

	/**
	 * The view of index page
	 */
	public static final String VIEW_INDEX = "wish/index";

	/**
	 * The view of addition page
	 */
	public static final String VIEW_ADD = "wish/add";

	/**
	 * The {@link WishService}
	 */
	@Autowired
	WishService wishService;

	/**
	 * The {@link WishHelper}
	 */
	@Autowired
	WishHelper wishHelper;
	
	/**
	 * The method for index page
	 * 
	 * @param pageable The {@link Pageable} instance for this index
	 * @param mav The {@link ModelAndView} instance for this method
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_INDEX)
	ModelAndView index(@PageableDefault(page=0, size=10) Pageable pageable, ModelAndView mav){
		Pagination<Wish> wishes = new Pagination<>(wishService.getWishesInPage(pageable.previousOrFirst()), PATH_INDEX);
		
		mav.setViewName(VIEW_INDEX);
		mav.addObject("wishes", wishes);
		
		return mav;
	}
	
	/**
	 * The method for addition form page
	 * 
	 * @param wishAdditionForm The {@link WishAdditionForm} instance
	 * @param mav The {@link ModelAndView} instance for this method
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_ADD, method=RequestMethod.GET)
	ModelAndView addForm(@ModelAttribute("wishAdditionForm") WishAdditionForm wishAdditionForm, ModelAndView mav){
		
		mav.setViewName(VIEW_ADD);
		
		// Build Priority list
		List<Priority> priorities = Arrays.asList(Priority.values());
		Collections.reverse(priorities);
		mav.addObject("priorities", priorities);
		
		// Set default Priority
		if (wishAdditionForm.getPriority() == null) {
			wishAdditionForm.setPriority(Priority.MEDIUM);
		}
		
		return mav;
	}
	
	/**
	 * The method for addition form action
	 * 
	 * @param wishAdditionForm The {@link WishAdditionForm} instance
	 * @param br The {@link BindingResult} instance
	 * @param mav The {@link ModelAndView} instance for this method
	 * @param loginUserDetails The {@link LoginUserDetails} of current login user
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_ADD, method=RequestMethod.POST)
	@Transactional(readOnly=false)
	ModelAndView add(@ModelAttribute("wishAdditionForm") @Validated WishAdditionForm wishAdditionForm, BindingResult br, ModelAndView mav, @LoginUser LoginUserDetails loginUserDetails){

		if (br.hasErrors()) {
			// Back to the form page
			return addForm(wishAdditionForm, mav);
		}else {
			// Save the new wish
			wishService.saveAndFlush(wishHelper.newEntity(wishAdditionForm, loginUserDetails.getWisher()));
			
			// Back to index page
			return new ModelAndView(redirect(PATH_INDEX));
		}
	}

	/**
	 * The method for deletion form action
	 * 
	 * @param wishDeletionForm The {@link WishDeletionForm} instance
	 * @param br The {@link BindingResult} instance
	 * @param mav The {@link ModelAndView} instance for this method
	 * @param loginUserDetails The {@link LoginUserDetails} of current login user
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_DELETE, method=RequestMethod.POST)
	@Transactional(readOnly=false)
	ModelAndView delete(@ModelAttribute("wishDeletionForm") @Validated WishDeletionForm wishDeletionForm, BindingResult br, ModelAndView mav, @LoginUser LoginUserDetails loginUserDetails){
		
		if (br.hasErrors()) {
			// No validation error occurred in normal operations
			return new ModelAndView(redirectToError());
		}
		
		// Delete the wish
		wishService.delete(wishDeletionForm.getId());
		
		// Back to index page
		return new ModelAndView(redirect(PATH_INDEX));
	}
}
