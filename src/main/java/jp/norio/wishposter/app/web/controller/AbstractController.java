package jp.norio.wishposter.app.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The abstract controller as base controller
 */
public class AbstractController {
	
	/**
	 * The logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractController.class);

	/**
	 * The prefix for redirect path
	 */
	protected static final String REDIRECT_PREFIX = "redirect:";
	
	/**
	 * The prefix for forward path
	 */
	protected static final String FORWARD_PREFIX = "forward:";
	
	/**
	 * The path of default home page
	 */
	protected static final String PATH_HOME = "/";
	
	/**
	 * The path of default error page
	 */
	protected static final String PATH_ERROR = "/error";

	/**
	 * Return the redirect path for viewName argument
	 * 
	 * @param viewName The view name for redirect
	 * @return The redirect path
	 */
	protected String redirect(String viewName) {
		return REDIRECT_PREFIX + viewName;
	}

	/**
	 * Return the redirect path for default home page
	 * 
	 * @return The redirect path for default home page
	 */
	protected String redirectToHome() {
		return redirect(PATH_HOME);
	}
	
	/**
	 * Return the redirect path for default error page
	 * 
	 * @return The redirect path for default error page
	 */
	protected String redirectToError() {
		return redirect(PATH_ERROR);
	}
	
	/**
	 * Return the forward path for viewName argument
	 * 
	 * @param viewName The view name for forward
	 * @return The forward path
	 */
	protected String forward(String viewName) {
		return FORWARD_PREFIX + viewName;
	}

	/**
	 * Return the forward path for default home page
	 * 
	 * @return The forward path for default home page
	 */
	protected String forwardToHome() {
		return forward(PATH_HOME);
	}
	
	/**
	 * Return the forward path for default error page
	 * 
	 * @return The forward path for default error page
	 */
	protected String forwardToError() {
		return forward(PATH_ERROR);
	}
	
}
