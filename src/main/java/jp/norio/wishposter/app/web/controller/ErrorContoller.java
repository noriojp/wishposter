package jp.norio.wishposter.app.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * The controller for error pages
 */
@Controller
public class ErrorContoller extends AbstractController{

	/**
	 * The path of default error page
	 */
	public static final String PATH_ERROR = "/error";

	/**
	 * The path of 404 error page
	 */
	public static final String PATH_404_ERROR = "/error/404";

	/**
	 * The path of 500 error page
	 */
	public static final String PATH_500_ERROR = "/error/500";

	/**
	 * The view of default error page
	 */
	public static final String VIEW_ERROR = "error";

	/**
	 * The view of 404 error page
	 */
	public static final String VIEW_404_ERROR = "error/404";

	/**
	 * The view of 500 error page
	 */
	public static final String VIEW_500_ERROR = "error/500";

	/**
	 * The logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(ErrorContoller.class);

	/**
	 * The method for 404 error page
	 * 
	 * @return The view of this method
	 */
	@RequestMapping(path=PATH_404_ERROR)
	String error404(){
		return VIEW_404_ERROR;
	}

	/**
	 * The method for 500 error page
	 * 
	 * @return The view of this method
	 */
	@RequestMapping(path=PATH_500_ERROR)
	String error500(){
		return VIEW_500_ERROR;
	}
}
