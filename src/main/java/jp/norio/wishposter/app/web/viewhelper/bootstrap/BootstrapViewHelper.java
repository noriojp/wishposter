package jp.norio.wishposter.app.web.viewhelper.bootstrap;

/**
 * The view helper for The Bootstrap
 */
public class BootstrapViewHelper {

	/**
	 * No instance
	 */
	private BootstrapViewHelper() {
	}

	/**
	 * Return the prefix with the word argument
	 * 
	 * @param word The word for prefix
	 * @return The prefix
	 */
	public static String prefixOf(String word) {
		return word + BootstrapConstants.CONCATENATOR;
	}

	/**
	 * Return the suffix with the word argument
	 * 
	 * @param word The word for suffix
	 * @return The suffix
	 */
	public static String suffixOf(String word) {
		return BootstrapConstants.CONCATENATOR + word;
	}

}
