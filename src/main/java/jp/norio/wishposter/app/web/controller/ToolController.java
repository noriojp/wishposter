package jp.norio.wishposter.app.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * The controller for tool pages
 */
@Controller
public class ToolController extends AbstractController{
	
	/**
	 * The logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger LOGGER = LoggerFactory.getLogger(ToolController.class);

	/**
	 * The path of {@link BCryptPasswordEncoder} driver page
	 */
	public static final String PATH_BCRYPT = "/tool/bcrypt";

	/**
	 * The view of {@link BCryptPasswordEncoder} driver page
	 */
	public static final String VIEW_BCRYPT = "tool/bcrypt";

	/**
	 * The method for {@link BCryptPasswordEncoder} driver page
	 * 
	 * @param password The password for {@link BCryptPasswordEncoder}
	 * @param modelAndView The {@link ModelAndView} instance for this method
	 * @return The {@link ModelAndView} instance of this method
	 */
	@RequestMapping(path=PATH_BCRYPT)
	ModelAndView bcrypt(@RequestParam(value="password", required=false)String password,
			ModelAndView modelAndView){
		
		modelAndView.setViewName(VIEW_BCRYPT);
		
		if (password != null) {
			// Encode the password and set to view
			modelAndView.addObject("encodedPassword", new BCryptPasswordEncoder().encode(password));
		}
		
		return modelAndView;
	}
}
