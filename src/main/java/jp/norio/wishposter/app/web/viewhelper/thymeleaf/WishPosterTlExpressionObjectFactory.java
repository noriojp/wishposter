package jp.norio.wishposter.app.web.viewhelper.thymeleaf;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

import org.thymeleaf.context.IExpressionContext;
import org.thymeleaf.expression.IExpressionObjectFactory;

import jp.norio.wishposter.app.web.viewhelper.bootstrap.BsTaskViewHelper;
import jp.norio.wishposter.app.web.viewhelper.bootstrap.BsWishViewHelper;
import jp.norio.wishposter.app.web.viewhelper.bootstrap.BsWisherViewHelper;

/**
 * Builds the expression objects to be used by Wish Poster dialects.
 */
public class WishPosterTlExpressionObjectFactory implements IExpressionObjectFactory {

	/** The object name for {@link BsWishViewHelper} */
    public static final String BS_WISH_VIEW_HELPER_EXPRESSION_OBJECT_NAME = "bsWish";
	/** The object name for {@link BsTaskViewHelper} */
    public static final String BS_TASK_VIEW_HELPER_EXPRESSION_OBJECT_NAME = "bsTask";
	/** The object name for {@link BsWisherViewHelper} */
    public static final String BS_WISHER_VIEW_HELPER_EXPRESSION_OBJECT_NAME = "bsWisher";
    
    /** The set of all expression names */
    private static final Set<String> ALL_EXPRESSION_OBJECT_NAMES =
            Collections.unmodifiableSet(new LinkedHashSet<String>(Arrays.asList(
                    new String[]{
                            BS_WISH_VIEW_HELPER_EXPRESSION_OBJECT_NAME,
                            BS_TASK_VIEW_HELPER_EXPRESSION_OBJECT_NAME,
                            BS_WISHER_VIEW_HELPER_EXPRESSION_OBJECT_NAME
                    }
            )));
    
	@Override
	public Set<String> getAllExpressionObjectNames() {
		return ALL_EXPRESSION_OBJECT_NAMES;
	}

	@Override
	public Object buildObject(IExpressionContext context, String expressionObjectName) {
		
		if (BS_WISH_VIEW_HELPER_EXPRESSION_OBJECT_NAME.equals(expressionObjectName)) {
			return new BsWishViewHelper();
		}
		
		if (BS_TASK_VIEW_HELPER_EXPRESSION_OBJECT_NAME.equals(expressionObjectName)) {
			return new BsTaskViewHelper();
		}
		
		if (BS_WISHER_VIEW_HELPER_EXPRESSION_OBJECT_NAME.equals(expressionObjectName)) {
			return new BsWisherViewHelper();
		}
		
		return null;
	}

	@Override
	public boolean isCacheable(String expressionObjectName) {
		return false;
	}

}
