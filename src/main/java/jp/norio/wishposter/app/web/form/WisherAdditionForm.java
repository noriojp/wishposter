package jp.norio.wishposter.app.web.form;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import jp.norio.wishposter.domain.Role;
import jp.norio.wishposter.domain.service.WisherService;
import jp.norio.wishposter.validator.constraints.FieldMatch;
import jp.norio.wishposter.validator.constraints.RoleEnum;
import jp.norio.wishposter.validator.constraints.Unique;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * The form for wisher addition
 */
@Data
@Accessors(fluent=false)
@FieldMatch(field="password", confirmer="passwordConfirmer")
public class WisherAdditionForm {

	/**
	 * Login ID of the wisher
	 */
	@NotEmpty
	@Size(max=255)
	@Unique(service=WisherService.class, methodName="findByLoginId")
	private String loginId;

	/**
	 * Login password of the wisher
	 */
	@NotEmpty
	@Size(max=255)
	private String password;

	/**
	 * Login password confirmation of the wisher
	 */
	@NotEmpty
	@Size(max=255)
	private String passwordConfirmer;

	/**
	 * The wisher name
	 */
	@NotEmpty
	@Size(max=255)
	private String name;

	/**
	 * Role of the wisher
	 */
	@NotNull
	@RoleEnum
	@Enumerated(EnumType.STRING)
	private Role role;

}
