package jp.norio.wishposter.context;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * The application context provider
 */
@Component
public class ApplicationContextProvider implements ApplicationContextAware {

	/**
	 * The application context
	 */
	private static ApplicationContext context;

	/**
	 * Set the {@link ApplicationContext} to this provider
	 */
	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		ApplicationContextProvider.context = context;
	}

	/**
	 * Return the bean specified by clazz argument
	 * 
	 * @param clazz {@link Class} of the bean
	 * @return The bean
	 */
	public static Object getBean(Class<?> clazz) {
		return ApplicationContextProvider.context.getBean(clazz);
	}

	/**
	 * Return the bean specified by qualifier and clazz argument
	 * 
	 * @param qualifier Qualifier of the bean
	 * @param clazz {@link Class} of the bean
	 * @return
	 */
	public static Object getBean(String qualifier, Class<?> clazz) {
		return ApplicationContextProvider.context.getBean(qualifier, clazz);
	}	
	
}
