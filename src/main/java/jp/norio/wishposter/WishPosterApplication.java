package jp.norio.wishposter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Wish Poster application main class
 */
@SpringBootApplication
public class WishPosterApplication {

	/**
	 * The main method for boot
	 * 
	 * @param args boot arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(WishPosterApplication.class, args);
	}
}
