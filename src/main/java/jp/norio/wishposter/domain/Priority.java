package jp.norio.wishposter.domain;

import lombok.Getter;
import lombok.experimental.Accessors;

/**
 * The enumeration of priorities
 */
@Getter
@Accessors(fluent=true)
public enum Priority {

	/**
	 * Critical
	 */
	CRITICAL("Critical"),

	/**
	 * High
	 */
	HIGH("High"),

	/**
	 * Medium
	 */
	MEDIUM("Medium"),
	
	/**
	 * Low
	 */
	LOW("Low"),
	;

	/**
	 * Label of the priority
	 */
	private final String label;

	/**
	 * The constructor
	 * 
	 * @param label Label of the priority
	 */
	private Priority(String label) {
		this.label = label;
	}
}
