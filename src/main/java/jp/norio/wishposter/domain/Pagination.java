package jp.norio.wishposter.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Page;

import com.google.common.collect.Lists;

import lombok.Value;
import lombok.experimental.Accessors;

/**
 * Pagination
 * 
 * @param <T> The element type
 */
@Value
@Accessors(fluent=true)
public class Pagination<T> {

	/**
	 * The default max pagination size
	 */
	private static final int DEFAULT_MAX_PAGINATION_SIZE = 5;

	/**
	 * {@link Page} instance for this pagination
	 */
	Page<T> page;
	
	/**
	 * The list of {@link PaginationItem}
	 */
	List<PaginationItem> paginationItems = new ArrayList<>();
	
	/**
	 * The current page number
	 */
	int currentPageNumber;
	
	/**
	 * The URL for pagination
	 */
	String url;

	/**
	 * The constructor
	 * 
	 * @param page The {@link Page} instance for pagination
	 * @param url The URL for pagination
	 */
	public Pagination(Page<T> page, String url){
		this(page, url, DEFAULT_MAX_PAGINATION_SIZE);
	}
	
	/**
	 * The constructor
	 * 
	 * @param page The {@link Page} instance for pagination
	 * @param url The URL for pagination
	 * @param maxPaginationSize The max pagination size
	 */
	public Pagination(Page<T> page, String url, int maxPaginationSize){
		this.page = page;
		this.url = url;
		
		currentPageNumber = page.getNumber() + 1;
		
		int startPageNumber, paginationSize;
		if (page.getTotalPages() <= maxPaginationSize) {
			startPageNumber = 1;
			paginationSize = page.getTotalPages();
		}else {
			if (currentPageNumber <= (maxPaginationSize - maxPaginationSize / 2)) {
				startPageNumber = 1;
			}else if (currentPageNumber >= (page.getTotalPages() - maxPaginationSize / 2)) {
				startPageNumber = page.getTotalPages() - maxPaginationSize + 1;
			}else {
				startPageNumber = currentPageNumber - (maxPaginationSize / 2);
			}
			paginationSize = maxPaginationSize;
		}
		
		for (int i = 0; i < paginationSize; i++) {
			int pageNumber = startPageNumber + i;
			boolean isCurrentPage = (pageNumber == currentPageNumber);
			paginationItems.add(new PaginationItem(pageNumber, isCurrentPage));
		}
	}
	
	/**
	 * Return elements in the page
	 * 
	 * @return Elements in the page
	 */
	public List<T> elements(){
		return page.getContent();
	}

	/**
	 * Return the total amount of elements
	 * 
	 * @return The total amount of elements
	 */
	public long totalElements(){
		return page.getTotalElements();
	}
	
	/**
	 * Return the total amount of pages
	 * 
	 * @return The total amount of pages
	 */
	public int totalPages(){
		return page.getTotalPages();
	}

	/**
	 * Return the current page number
	 * 
	 * @return The current page number
	 */
	public int pageNumber(){
		return page.getNumber();
	}
	
	/**
	 * Return total amount of elements in the page
	 * 
	 * @return Total amount of elements in the page
	 */
	public int pageSize(){
		return page.getSize();
	}
	
	/**
	 * Returns whether the current page is the first one
	 * 
	 * @return Whether the current page is the first one
	 */
	public boolean isFirstPage() {
		return page.isFirst();
	}

	/**
	 * Returns whether the current page is the last one
	 * 
	 * @return Whether the current page is the last one
	 */
	public boolean isLastPage() {
		return page.isLast();
	}

	/**
	 * Returns if there is a previous page
	 * 
	 * @return If there is a previous page
	 */
	public boolean hasPreviousPage() {
		return page.hasPrevious();
	}
	
	/**
	 * Returns if there is a next page
	 * 
	 * @return If there is a next page
	 */
	public boolean hasNextPage() {
		return page.hasNext();
	}
	
	/**
	 * Return the partition of page
	 * 
	 * @param size partition size
	 * @return The partition of page
	 */
	public List<List<T>> partitionedPages(int size){
		return Lists.partition(Lists.newArrayList(page), size);
	}
	
	/**
	 * Return the partition of page<br />
	 * Alias for <code>partitionedPages()</code>
	 * 
	 * @param size partition size
	 * @return The partition of page
	 */
	public List<List<T>> subPages(int size){
		return partitionedPages(size);
	}

	/**
	 * Pagination Item
	 */
	@Value
	@Accessors(fluent=true)
	public class PaginationItem{
		/**
		 * Number of the item
		 */
		int number;
		
		/**
		 * Whether the item is current page number
		 */
		boolean isCurrent;
	}
}
