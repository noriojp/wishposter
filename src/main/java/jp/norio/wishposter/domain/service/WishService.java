package jp.norio.wishposter.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jp.norio.wishposter.domain.entity.Wish;
import jp.norio.wishposter.domain.repository.WishRepository;

/**
 * The service for wishes
 */
@Service
public class WishService {

	/**
	 * The {@link WishRepository}
	 */
	@Autowired
	WishRepository wishRepository;
	
	/**
	 * Return wishes in the page of {@link Pageable}
	 * 
	 * @param pageable The {@link Pageable}
	 * @return wishes in the page of {@link Pageable}
	 */
	public Page<Wish> getWishesInPage(Pageable pageable){
		return wishRepository.findAll(pageable);
	}
	
	/**
	 * Save the {@link Wish}
	 * 
	 * @param wish The {@link Wish}
	 */
	public void saveAndFlush(Wish wish){
		wishRepository.saveAndFlush(wish);
	}
	
	/**
	 * Delete the {@link Wish}
	 * 
	 * @param id ID of the {@link Wish}
	 */
	public void delete(Long id) {
		wishRepository.deleteById(id);
	}
}
