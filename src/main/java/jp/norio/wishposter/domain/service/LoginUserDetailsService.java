package jp.norio.wishposter.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import jp.norio.wishposter.domain.entity.LoginUserDetails;
import jp.norio.wishposter.domain.entity.Wisher;
import jp.norio.wishposter.domain.repository.WisherRepository;

/**
 * The service for login user details
 */
@Service
public class LoginUserDetailsService implements UserDetailsService {

	/**
	 * The {@link WisherRepository}
	 */
	@Autowired
	WisherRepository wisherRepository;
	
	/**
	 * Return the {@link UserDetails} by the username argument
	 * 
	 * @param username The user name for {@link UserDetails}
	 * @return The {@link UserDetails}
	 */
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Wisher wisher = null;
		try {
			wisher = wisherRepository.findByLoginId(username);
		} catch (Exception e) {
			throw new UsernameNotFoundException(String.format("[login id:%s]", username), e);
		}
		
		if (wisher == null) {
			throw new UsernameNotFoundException(String.format("unknown user[login id:%s]", username));
		}
		return new LoginUserDetails(wisher);
	}

}
