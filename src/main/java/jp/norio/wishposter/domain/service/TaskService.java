package jp.norio.wishposter.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jp.norio.wishposter.domain.entity.Task;
import jp.norio.wishposter.domain.repository.TaskRepository;

/**
 * The service for tasks
 */
@Service
public class TaskService {

	/**
	 * The {@link TaskRepository}
	 */
	@Autowired
	TaskRepository taskRepository;

	/**
	 * Return tasks in the page of {@link Pageable}
	 * 
	 * @param pageable The {@link Pageable}
	 * @return tasks in the page of {@link Pageable}
	 */
	public Page<Task> getTasksInPage(Pageable pageable){
		return taskRepository.findAll(pageable);
	}

	/**
	 * Save the {@link Task}
	 * 
	 * @param task The {@link Task}
	 */
	public void saveAndFlush(Task task){
		taskRepository.saveAndFlush(task);
	}
	
	/**
	 * Delete the {@link Task} by ID
	 * 
	 * @param id ID of the {@link Task}
	 */
	public void delete(Long id) {
		taskRepository.deleteById(id);
	}
	
}
