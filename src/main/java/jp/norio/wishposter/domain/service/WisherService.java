package jp.norio.wishposter.domain.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import jp.norio.wishposter.domain.entity.Wisher;
import jp.norio.wishposter.domain.repository.WisherRepository;

/**
 * The service for wishers
 */
@Service
public class WisherService {

	/**
	 * The {@link WisherRepository}
	 */
	@Autowired
	WisherRepository wisherRepository;
	
	/**
	 * Return wishers in the page of {@link Pageable}
	 * 
	 * @param pageable The {@link Pageable}
	 * @return wishers in the page of {@link Pageable}
	 */
	public Page<Wisher> getWishersInPage(Pageable pageable){
		return wisherRepository.findAll(pageable);
	}

	/**
	 * Return the {@link Wisher} by the login ID
	 * 
	 * @param loginId The login ID
	 * @return The {@link Wisher}
	 */
	public Wisher findByLoginId(String loginId){
		return wisherRepository.findByLoginId(loginId);
	}

	/**
	 * Save the {@link Wisher}
	 * 
	 * @param wisher The {@link Wisher}
	 */
	public void saveAndFlush(Wisher wisher){
		wisherRepository.saveAndFlush(wisher);
	}
	
	/**
	 * Delete the {@link Wisher}
	 * 
	 * @param id ID of the {@link Wisher}
	 */
	public void delete(Long id) {
		wisherRepository.deleteById(id);
	}
	
}
