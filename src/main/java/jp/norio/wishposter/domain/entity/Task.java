package jp.norio.wishposter.domain.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import org.springframework.format.annotation.DateTimeFormat;

import jp.norio.wishposter.domain.Priority;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * The entity of task
 */
@Entity
@Data
@Accessors(fluent = true)
public class Task {

	/**
	 * ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long id;
	
	/**
	 * Name of the task
	 */
	@Column(length = 255, nullable = false)
	private String name;

	/**
	 * Priority of the task
	 */
	@Enumerated(EnumType.STRING)
	@Column(length = 16, nullable = false)
	private Priority priority;
	
	/**
	 * Deadline of the task
	 */
	@DateTimeFormat(pattern="yyyy-MM-dd")
	@Column(nullable = true)
	private Timestamp deadline;

	/**
	 * Comment for the task
	 */
	@Column(length = 255, nullable = true)
	private String comment;
	
	/**
	 * Owner wisher of the task
	 */
	@ManyToOne
	private Wisher wisher;

	/**
	 * Creation time stamp
	 */
	@Generated(GenerationTime.INSERT)
	@Column(insertable = false, updatable = false)
	private Timestamp createdAt;
	
	/**
	 * Update time stamp
	 */
	@Generated(GenerationTime.ALWAYS)
	@Column(insertable = false, updatable = false)
	private Timestamp updatedAt;

	/**
	 * Version
	 */
	@Version
	private long version;
	
}
