package jp.norio.wishposter.domain.entity;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import jp.norio.wishposter.domain.Role;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * The entity of wisher
 */
@Entity
@Data
@Accessors(fluent = true)
public class Wisher {

	/**
	 * ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column
	private long id;
	
	/**
	 * Login ID of the wisher
	 */
	@Column(length = 255, nullable = false, unique=true)
	private String loginId;
	
	/**
	 * Encoded login password of the wisher
	 */
	@Column(length = 255, nullable = false, name = "password")
	private String encodedPassword;
	
	/**
	 * Name of the wisher
	 */
	@Column(length = 255, nullable = false)
	private String name;

	/**
	 * Role of the wisher
	 */
	@Enumerated(EnumType.STRING)
	@Column(length = 16, nullable = false)
	private Role role;
	
	/**
	 * Creation time stamp
	 */
	@Generated(GenerationTime.INSERT)
	@Column(insertable = false, updatable = false)
	private Timestamp createdAt;
	
	/**
	 * Update time stamp
	 */
	@Generated(GenerationTime.ALWAYS)
	@Column(insertable = false, updatable = false)
	private Timestamp updatedAt;
	
	/**
	 * Version
	 */
	@Version
	private long version;

	/**
	 * Wishes added by the wisher
	 */
	@OneToMany(mappedBy = "wisher")
	private List<Wish> wishes;

	/**
	 * Tasks added by the wisher
	 */
	@OneToMany(mappedBy = "wisher")
	private List<Task> tasks;

}
