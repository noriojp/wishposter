package jp.norio.wishposter.domain.entity;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * The entity of login user details for Spring Security 
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(fluent = false)
@SuppressWarnings("serial")
public class LoginUserDetails extends User {

	/**
	 * Wisher entity of the current login user
	 */
	private Wisher wisher;
	
	/**
	 * Display name of the current login user
	 */
	private String name;
	
	/**
	 * The constructor
	 * 
	 * @param wisher Wisher entity of the login user
	 */
	public LoginUserDetails(Wisher wisher) {
		super(wisher.loginId(), wisher.encodedPassword(), AuthorityUtils.createAuthorityList(wisher.role().roleWithPrefix()));
		
		this.wisher = wisher;
		name = wisher.name();
	}

}
