package jp.norio.wishposter.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jp.norio.wishposter.domain.entity.Wish;

/**
 * The repository for wishes
 */
@Repository
public interface WishRepository extends JpaRepository<Wish, Long> {

}
