package jp.norio.wishposter.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jp.norio.wishposter.domain.entity.Task;

/**
 * The repository for tasks
 */
@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

}
