package jp.norio.wishposter.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jp.norio.wishposter.domain.entity.Wisher;

/**
 * The repository for wishers
 */
@Repository
public interface WisherRepository extends JpaRepository<Wisher, Long> {

	public Wisher findByLoginId(String loginId);
}
