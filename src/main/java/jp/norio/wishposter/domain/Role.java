package jp.norio.wishposter.domain;

import lombok.Getter;
import lombok.experimental.Accessors;

/**
 * The enumeration of roles
 */
@Accessors(fluent=true)
@Getter
public enum Role {

	/**
	 * User
	 */
	USER,

	/**
	 * Administrator
	 */
	ADMIN,
	;

	/**
	 * The prefix of role for Spring Security
	 */
	private static final String PREFIX = "ROLE_";
	
	/**
	 * The role with prefix
	 */
	private final String roleWithPrefix;

	/**
	 * The constructor
	 */
	private Role() {
		roleWithPrefix = PREFIX + name();
	}
}
