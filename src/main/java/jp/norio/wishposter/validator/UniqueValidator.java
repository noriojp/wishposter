package jp.norio.wishposter.validator;

import java.lang.reflect.Method;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import jp.norio.wishposter.context.ApplicationContextProvider;
import jp.norio.wishposter.validator.constraints.Unique;

/**
 * The validator of {@link Unique}
 */
public class UniqueValidator implements ConstraintValidator<Unique, Object> {

	/**
	 * The service class for unique check
	 */
	private Class<?> serviceClass;
	
	/**
	 * The qualifier of the service class for unique check
	 */
	private String qualifier;
	
	/**
	 * The method name of unique check
	 */
	private String methodName;
	
	/**
	 * The key type of argument of the method of unique check
	 */
	private Class<?> keyType;
	
	/**
	 * The service instance for unique check
	 */
	private Object service;
	
	/**
	 * The method of unique check
	 */
	private Method method;

	/**
	 * Initialize the {@link Unique} annotation
	 * 
	 * @param unique The {@link Unique} annotation
	 */
	@Override
	public void initialize(Unique unique) {
		serviceClass = unique.service();
		qualifier = unique.qualifier();
		methodName = unique.methodName();
		keyType = unique.keyType();
		
		try {
			// Get the service instance
			if (StringUtils.isEmpty(qualifier)) {
				service = ApplicationContextProvider.getBean(serviceClass);
			}else {
				service = ApplicationContextProvider.getBean(qualifier, serviceClass);
			}

			// Get the method
			method = ReflectionUtils.findMethod(serviceClass, methodName, keyType);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Return whether the input argument is valid
	 * 
	 * @param object The input for validation
	 * @param context The context of the validator
	 * @return Whether the input argument is valid 
	 */
	@Override
	public boolean isValid(Object input, ConstraintValidatorContext context) {
		if (input == null) {
			return true;
		}
		
		try {
			// Find the same object
			if (method.invoke(service, input) == null) {
				return true;
			}
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}

		return false;
	}

}
