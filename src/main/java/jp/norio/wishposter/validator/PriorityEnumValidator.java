package jp.norio.wishposter.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import jp.norio.wishposter.domain.Priority;
import jp.norio.wishposter.validator.constraints.PriorityEnum;

/**
 * The validator of {@link PriorityEnum} annotation
 */
public class PriorityEnumValidator implements ConstraintValidator<PriorityEnum, Priority> {

	/**
	 * Initialize the validator
	 * 
	 * @param priorityEnum The {@link PriorityEnum} annotation
	 */
	@Override
	public void initialize(PriorityEnum priorityEnum) {
	}

	/**
	 * Return whether the input argument is valid
	 * 
	 * @param object The input for validation
	 * @param context The context of the validator
	 * @return Whether the input argument is valid 
	 */
	@Override
	public boolean isValid(Priority input, ConstraintValidatorContext context) {
		return input != null;
	}

}
