package jp.norio.wishposter.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import jp.norio.wishposter.domain.Role;
import jp.norio.wishposter.validator.constraints.RoleEnum;

/**
 * The validator of {@link RoleEnum} annotation
 */
public class RoleEnumValidator implements ConstraintValidator<RoleEnum, Role> {

	/**
	 * Initialize the {@link RoleEnum} annotation
	 * 
	 * @param roleEnum The {@link RoleEnum} annotation
	 */
	@Override
	public void initialize(RoleEnum roleEnum) {
	}

	/**
	 * Return whether the input argument is valid
	 * 
	 * @param object The input for validation
	 * @param context The context of the validator
	 * @return Whether the input argument is valid 
	 */
	@Override
	public boolean isValid(Role input, ConstraintValidatorContext context) {
		return input != null;
	}

}
