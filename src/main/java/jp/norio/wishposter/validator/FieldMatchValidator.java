package jp.norio.wishposter.validator;

import java.lang.reflect.Field;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.ReflectionUtils;

import jp.norio.wishposter.validator.constraints.FieldMatch;

/**
 * The validator of {@link FieldMatch} annotation
 */
public class FieldMatchValidator implements ConstraintValidator<FieldMatch, Object> {
	
	/**
	 * The field name
	 */
	private String fieldName;
	
	/**
	 * The field name of confirmation
	 */
	private String confirmationFieldName;
	
	/**
	 * The validation error message
	 */
	private String message;

	/**
	 * Initialize the validator
	 * 
	 * @param fieldMatch The {@link FieldMatch} annotation
	 */
	@Override
	public void initialize(FieldMatch fieldMatch) {
		fieldName = fieldMatch.field();
		confirmationFieldName = fieldMatch.confirmer();
		message = fieldMatch.message();
	}

	/**
	 * Return whether the object argument is valid
	 * 
	 * @param object The object for validation
	 * @param context The context of the validator
	 * @return Whether the object argument is valid 
	 */
	@Override
	public boolean isValid(Object object, ConstraintValidatorContext context) {
		boolean isValid = false;
		try {
			// Get the field values
			Field field = ReflectionUtils.findField(object.getClass(), fieldName);
			ReflectionUtils.makeAccessible(field);
			Object fieldValue = field.get(object);
			Field confirmationField = ReflectionUtils.findField(object.getClass(), confirmationFieldName);
			ReflectionUtils.makeAccessible(confirmationField);
			Object confirmationFieldValue = confirmationField.get(object);
			
			// Compare the values
			isValid = (fieldValue == null && confirmationFieldValue == null) || (fieldValue != null && fieldValue.equals(confirmationFieldValue));
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		
		if (!isValid) {
			// Set the validation error
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate(message).addPropertyNode(fieldName).addConstraintViolation();
		}
		
		return isValid;
	}

}
