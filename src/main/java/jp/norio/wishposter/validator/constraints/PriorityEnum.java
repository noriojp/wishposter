package jp.norio.wishposter.validator.constraints;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;

import jp.norio.wishposter.domain.Priority;
import jp.norio.wishposter.validator.PriorityEnumValidator;

/**
 * The annotation for {@link Priority} check
 */
@Documented
@Constraint(validatedBy=PriorityEnumValidator.class)
@Target({METHOD, FIELD})
@Retention(RUNTIME)
@ReportAsSingleViolation
public @interface PriorityEnum {

	/**
	 * Return the error messsage
	 * 
	 * @return The error message
	 */
	String message() default "{jp.norio.wishposter.validator.constraints.PriorityEnum.message}";
	
	/**
	 * Return the group
	 * 
	 * @return The group
	 */
	Class<?>[] groups() default {};
	
	/**
	 * Return the {@link Payload}
	 * 
	 * @return The {@link Payload}
	 */
	Class<? extends Payload>[] payload() default {};
}
