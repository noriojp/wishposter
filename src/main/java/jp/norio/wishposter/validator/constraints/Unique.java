package jp.norio.wishposter.validator.constraints;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;

import jp.norio.wishposter.validator.UniqueValidator;

/**
 * The annotation for unique check
 */
@Documented
@Constraint(validatedBy=UniqueValidator.class)
@Target({METHOD, FIELD})
@Retention(RUNTIME)
@ReportAsSingleViolation
public @interface Unique {

	/**
	 * Return the error messsage
	 * 
	 * @return The error message
	 */
    String message() default "{jp.norio.wishposter.validator.constraints.Unique.message}";
	
	/**
	 * Return the group
	 * 
	 * @return The group
	 */
    Class<?>[] groups() default {};
	
	/**
	 * Return the {@link Payload}
	 * 
	 * @return The {@link Payload}
	 */
    Class<? extends Payload>[] payload() default {};
    
    /**
     * Return the service class for unique check
     * 
     * @return The service class for unique check
     */
    Class<?> service();

    /**
     * Return the qualifier of service class
     * 
     * @return The qualifier of service class
     */
    String qualifier() default "";
    
    /**
     * Return the method name of unique check
     * 
     * @return The method name of unique check
     */
    String methodName();
    
    /**
     * Return the key type of argument of the method of unique check
     * 
     * @return The key type of argument of the method of unique check
     */
    Class<?> keyType() default String.class;
}
