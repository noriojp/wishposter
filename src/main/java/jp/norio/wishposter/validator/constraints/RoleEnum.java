package jp.norio.wishposter.validator.constraints;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;
import javax.validation.ReportAsSingleViolation;

import jp.norio.wishposter.domain.Role;
import jp.norio.wishposter.validator.RoleEnumValidator;

/**
 * The annotation for {@link Role} check
 */
@Documented
@Constraint(validatedBy=RoleEnumValidator.class)
@Target({METHOD, FIELD})
@Retention(RUNTIME)
@ReportAsSingleViolation
public @interface RoleEnum {

	/**
	 * Return the error messsage
	 * 
	 * @return The error message
	 */
	String message() default "{jp.norio.wishposter.validator.constraints.RoleEnum.message}";
	
	/**
	 * Return the group
	 * 
	 * @return The group
	 */
	Class<?>[] groups() default {};
	
	/**
	 * Return the {@link Payload}
	 * 
	 * @return The {@link Payload}
	 */
	Class<? extends Payload>[] payload() default {};
}
