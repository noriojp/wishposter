package jp.norio.wishposter.validator.constraints;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import jp.norio.wishposter.validator.FieldMatchValidator;

/**
 * The annotation for cross field check
 */
@Documented
@Retention(RUNTIME)
@Target({ TYPE, ANNOTATION_TYPE })
@Constraint(validatedBy=FieldMatchValidator.class)
public @interface FieldMatch {

	/**
	 * Return the error messsage
	 * 
	 * @return The error message
	 */
	String message() default "{jp.norio.wishposter.validator.constraints.FieldMatch.message}";
	
	/**
	 * Return the group
	 * 
	 * @return The group
	 */
	Class<?>[] groups() default {};
	
	/**
	 * Return the {@link Payload}
	 * 
	 * @return The {@link Payload}
	 */
	Class<? extends Payload>[] payload() default {};
	
	/**
	 * Return the field name
	 * 
	 * @return The field name
	 */
	String field();
	
	/**
	 * Return the field name of confirmation
	 * 
	 * @return The field name of confirmation
	 */
	String confirmer();

	/**
	 * List of {@link FieldMatch} annotation
	 */
	@Target({TYPE, ANNOTATION_TYPE})
    @Retention(RUNTIME)
    @Documented
	public @interface List{
		FieldMatch[] value();
	}
}
