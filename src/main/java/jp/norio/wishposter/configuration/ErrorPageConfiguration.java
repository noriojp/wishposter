package jp.norio.wishposter.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.server.ErrorPageRegistrar;
import org.springframework.boot.web.server.ErrorPageRegistry;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import jp.norio.wishposter.app.web.controller.ErrorContoller;

/**
 * TODO
 *
 */
@Component
public class ErrorPageConfiguration implements ErrorPageRegistrar {

	@Override
	public void registerErrorPages(ErrorPageRegistry registry) {
		List<ErrorPage> errorPages = new ArrayList<>();
		ErrorPage notFound = new ErrorPage(HttpStatus.NOT_FOUND, ErrorContoller.PATH_404_ERROR);
		ErrorPage internalServerError = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, ErrorContoller.PATH_500_ERROR);
		errorPages.add(notFound);
		errorPages.add(internalServerError);
		registry.addErrorPages(errorPages.toArray(new ErrorPage[0]));

	}

}
