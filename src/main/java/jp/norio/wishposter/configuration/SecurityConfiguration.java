package jp.norio.wishposter.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import jp.norio.wishposter.app.web.controller.HomeController;
import jp.norio.wishposter.app.web.controller.LoginController;
import jp.norio.wishposter.domain.Role;

/**
 * The security configuration for this application
 */
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	/**
	 * The paths for public
	 */
	private static final String[] PUBLIC_PATHS = {"/tool/**", "/error/**", "/webjars/**", "/images/**", "/css/**", "/js/**", "/vendor/**"};

	/**
	 * The paths for administrator only
	 */
	private static final String[] ADMIN_PATHS = {"/admin/**"};
	
	/**
	 * The path of login form page
	 */
	private static final String LOGIN_FORM_PATH = LoginController.PATH_LOGIN;
	
	/**
	 * The path of login action
	 */
	private static final String LOGIN_PATH = LoginController.PATH_LOGIN;
	
	/**
	 * The path for login failure
	 */
	private static final String LOGIN_FAILURE_PATH = LoginController.PATH_LOGIN + "?error";
	
	/**
	 * The path for login success
	 */
	private static final String LOGIN_SUCCESS_DEFAULT_PATH = HomeController.PATH_HOME;
	
	/**
	 * The parameter name for login ID
	 */
	private static final String LOGIN_PARAMETER_USERNAME = "loginId";
	
	/**
	 * The parameter name for login password
	 */
	private static final String LOGIN_PARAMETER_PASSWORD = "password";
	
	/**
	 * The path of logout action
	 */
	private static final String LOGOUT_PATH = "/logout**";
	
	/**
	 * The path for logout success
	 */
	private static final String LOGOUT_SUCCESS_PATH = LoginController.PATH_LOGIN;

	/**
	 * Configure the {@link WebSecurity}
	 */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers(PUBLIC_PATHS);
	}
	
	/**
	 * Configure the {@link HttpSecurity}
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		// Configure the path permission
		http.authorizeRequests()
			.antMatchers(LOGIN_FORM_PATH).permitAll()
			.antMatchers(ADMIN_PATHS).hasRole(Role.ADMIN.toString())
			.anyRequest().authenticated();

		// Configure the login
		http.formLogin()
			.loginProcessingUrl(LOGIN_PATH)
			.loginPage(LOGIN_FORM_PATH)
			.failureUrl(LOGIN_FAILURE_PATH)
			.defaultSuccessUrl(LOGIN_SUCCESS_DEFAULT_PATH, true)
			.usernameParameter(LOGIN_PARAMETER_USERNAME)
			.passwordParameter(LOGIN_PARAMETER_PASSWORD);
		
		// Configure the logout
		http.logout()
			.logoutRequestMatcher(new AntPathRequestMatcher(LOGOUT_PATH))
			.logoutSuccessUrl(LOGOUT_SUCCESS_PATH);
	}

	/**
	 * The authentication configuration for this application
	 */
	@Configuration
	static class AuthenticationConfiguration extends GlobalAuthenticationConfigurerAdapter{

		/**
		 * The {@link UserDetailsService}
		 */
		@Autowired
		UserDetailsService userDetailsService;

		/**
		 * Return the {@link PasswordEncoder} instance for this application
		 * 
		 * @return the {@link PasswordEncoder} instance
		 */
		@Bean
		PasswordEncoder passwordEncoder(){
			return new BCryptPasswordEncoder();
		}
		
		/**
		 * Initialize the {@link AuthenticationManagerBuilder}
		 * 
		 * @param auth The {@link AuthenticationManagerBuilder}
		 * @throws Exception When an error occurred in {@link UserDetailsService}
		 */
		@Override
		public void init(AuthenticationManagerBuilder auth) throws Exception {
			
			auth
				.userDetailsService(userDetailsService)
				.passwordEncoder(passwordEncoder());
		}
	}
	
	
	
}
