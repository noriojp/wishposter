CREATE TABLE wisher(
	id BIGSERIAL PRIMARY KEY,
	login_id VARCHAR(255),
	password VARCHAR(255),
	name VARCHAR(255),
	role VARCHAR(16),
	created_at TIMESTAMP,
	updated_at TIMESTAMP,
	version BIGINT
);

CREATE TABLE wish(
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(255),
	quantity INT,
	priority VARCHAR(16),
	comment VARCHAR(255),
	wisher_id BIGINT,
	created_at TIMESTAMP,
	updated_at TIMESTAMP,
	version BIGINT
);

CREATE TABLE task(
	id BIGSERIAL PRIMARY KEY,
	name VARCHAR(255),
	priority VARCHAR(16),
	deadline TIMESTAMP,
	comment VARCHAR(255),
	wisher_id BIGINT,
	created_at TIMESTAMP,
	updated_at TIMESTAMP,
	version BIGINT
);

insert into wisher (login_id,password,name,role,created_at,updated_at,version) values('admin','$2a$10$NJv.Mr4MvF0ua8C4wq27KOlfK/7f7aYIgnRdb6y4CutZc0tb4r.EG','Admin','ADMIN',now(),now(),1);
insert into wisher (login_id,password,name,role,created_at,updated_at,version) values('user','$2a$10$NJv.Mr4MvF0ua8C4wq27KOlfK/7f7aYIgnRdb6y4CutZc0tb4r.EG','User','USER',now(),now(),1);

insert into wish (name,quantity,priority,comment,wisher_id,created_at,updated_at,version) values('84-inch TV',1,'LOW','Too expensive...',1, now(),now(),1);
insert into wish (name,quantity,priority,comment,wisher_id,created_at,updated_at,version) values('Toilet paper',2,'CRITICAL','Only a little bit paper left',2, now(),now(),1);
insert into wish (name,quantity,priority,comment,wisher_id,created_at,updated_at,version) values('Chili oil',1,'HIGH','Out of stock',2, now(),now(),1);
insert into wish (name,quantity,priority,comment,wisher_id,created_at,updated_at,version) values('Beer',1,'CRITICAL','Wanna drink this coming weekend',1, now(),now(),1);
