http_path = "/"
css_dir = "src/main/resources/static/css"
sass_dir = "scss"
images_dir = "/"
javascripts_dir = "js"
output_style = (environment == :production) ? :compressed : :expanded
line_comments = (environment == :production) ? false : true
